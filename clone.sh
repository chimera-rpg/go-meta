#!/bin/bash
git clone https://github.com/chimera-rpg/go-client src/client
git clone https://github.com/chimera-rpg/go-server src/server
git clone https://github.com/chimera-rpg/go-common src/common
git clone https://github.com/chimera-rpg/client-data share/chimera/client
git clone https://github.com/chimera-rpg/archetypes share/chimera/archetypes
git clone https://github.com/chimera-rpg/maps share/chimera/maps
